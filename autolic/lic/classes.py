from os.path import dirname
from threading import Thread
from subprocess import Popen, PIPE, Popen

class MyThread(Thread):
	content = None
	package = None
	proc = None
	path = ""
	def __init__(self, path):
		super().__init__()
		self.path = path
		args = ['/opt/1C/1CE/x86_64/ring/ring', 'license', 'list', '--path', f'{dirname(self.path)}']
		self.proc = Popen(args, stdout=PIPE, stderr=PIPE)

	def run(self):
		self.proc.wait()
		temp = self.proc.communicate()[0].decode("utf8").replace('\r\n', '').split('-')
		temp[1] = temp[1].split(' ')[0]
		self.package = temp
		if not all(self.package):
			self.package = ['']

		if self.package == ['']:
			self.package = ['000000000000000', 'Неизвестно']
			self.content = 'Ошибка чтения файла.<br>Возможно файл не является файлом лицензии 1С.<br>Попробуйте открыть файл lic блокнотом.'
		else:
			self.proc = Popen(['/opt/1C/1CE/x86_64/ring/ring', 'license', 'info', '--name', f"{self.package[0]}-{self.package[1]}", '--path', f'{dirname(self.path)}'], stdout=PIPE, stderr=PIPE)
			self.proc.wait()
			self.content = self.proc.communicate()[0].decode("utf-8").replace('\n    ', '<br>').replace('\n', '<br>')
