from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from .forms import UploadFileForm
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.forms import ValidationError
from .classes import MyThread

import os
import subprocess
import tempfile
import shutil

def list(request):
	# Handle file upload
	os.chdir(settings.BASE_DIR)
	if request.method == 'POST':
		form = UploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			content_tree = []
			new_files_list = []
			for file in request.FILES.getlist('docfile'):
				temp_name = next(tempfile._get_candidate_names())
				new_file_name = os.path.join(settings.FILES_URL, temp_name, file.name)
				path = default_storage.save(new_file_name, ContentFile(file.read()))
				new_files_list.append(new_file_name)
			procs = [MyThread(i) for i in new_files_list]
			for i in procs:	
				i.start()

			for i in procs:
				i.join()
				content_tree.append({
					'file' : os.path.basename(i.path),
					'package' : i.package[1],
					'pincode' : '-'.join([i.package[0][x:x + 3] for x in range(0, 15, 3)]),
					'content' : i.content
				})			
	
			for i in new_files_list:
				shutil.rmtree(os.path.dirname(i), ignore_errors=True)
			
			return render(request, 'lic_result.html', { "content_tree" : content_tree })
		else:
			return render(request, 'notfound404.html', { "errors" : form.errors })
	else:
		form = UploadFileForm()
	return render(request, 'lic_upload.html', { 'form' : form })
