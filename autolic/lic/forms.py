from django import forms
import json


class UploadFileForm(forms.Form):

	docfile = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), label='Выберите файл программной лицензии LIC',
		help_text='макс. вес - 10 КБ, макс. файлов - 25') 

	def clean_docfile(self):
		if len(self.files.getlist('docfile')) > 25:
			raise forms.ValidationError("Вы не можете загрузить больше 25ти файлов!")
		for file in self.files.getlist('docfile'):
			if file.size > 10000:
				raise forms.ValidationError(f"Файл {file} ну слишком большой!")
			if not file.name.endswith('.lic'):
				raise forms.ValidationError("Загружать можно только файлы .LIC")
