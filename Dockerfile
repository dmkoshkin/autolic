FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /1c_distr/ /1c_distr/
RUN apt update
RUN apt -y install default-jre
RUN apt -y install libwebkitgtk-3.0-0
RUN dpkg -i /1c_distr/1c-enterprise83-common_8.3.13-1809_amd64.deb
RUN dpkg -i /1c_distr/1c-enterprise83-server_8.3.13-1809_amd64.deb
RUN dpkg -i /1c_distr/1c-enterprise83-client_8.3.13-1809_amd64.deb
RUN dpkg -i /opt/1C/v8.3/x86_64/ExtDst/1c-enterprise-license-tools_0.10.0-1_amd64.deb
RUN dpkg -i /opt/1C/v8.3/x86_64/ExtDst/1c-enterprise-ring_0.11.5-3_amd64.deb
RUN rm -r /1c_distr/
ADD /config/req.pip /config/
RUN pip install -r /config/req.pip
RUN mkdir /autolic
WORKDIR /autolic